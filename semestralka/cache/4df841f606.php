<?php
// source: register.latte

use Latte\Runtime as LR;

class Template4df841f606 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>LOGIN<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-0"></div>
            <div class="col-md-4 col-lg-4 col-sm-12">
                <h1 class="display-4">Registrace</h1>
                <br>
                <form action="<?php
		echo $router->pathFor("register");
?>" method="post">
                    <label for="login">Uživatelké jméno</label> <br>
                    <input class="form-control" type="text" name="login" required> <br>
                    <label for="pass">Heslo</label> <br>
                    <input class="form-control" type="password" name="pass" required> <br>
                    <label for="pass">Heslo znovu</label> <br>
                    <input class="form-control" type="password" name="pass" required> <br>
                    <input class="btn btn-outline-info" type="submit" value="Zaregistrovat">
                </form>
            </div>
        </div>
    </div>
<?php
	}

}
