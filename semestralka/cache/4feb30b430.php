<?php
// source: index.latte

use Latte\Runtime as LR;

class Template4feb30b430 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['person'])) trigger_error('Variable $person overwritten in foreach on line 35');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>TITLE OF PAGE<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1>BODY CONTENT</h1>
                <br>

                <form action="<?php
		echo $router->pathFor("index");
?>" method="get">
                    <div class="form-group">
                        <label for="filter">Seznam lze vyfiltrovat dle příjmení:</label>
                        <br>
                        <input id="filter" type="text" name="filter">
                    </div>
                    <input type="submit" value="Filtruj" class="btn btn-secondary">
                </form>

                <br>
            </div>

            <div class="col-md-8">
                <table class="table table-striped table-hover table-border" border="1">
                    <tr>
                        <th><b>Jméno</b></th>
                        <th></b>Příjmení</b></th>
                        <th><b>Adresa</b></th>
                        <th><b>Počet kontaktů</b></th>
                        <th><b>Editovat</b></th>
                        <th><b>Odstranit</b></th>
                        
                    </tr>
<?php
		$iterations = 0;
		foreach ($people as $person) {
?>
                        <tr>
                            <td><?php echo LR\Filters::escapeHtmlText($person['first_name']) /* line 37 */ ?></td>
                            <td><?php echo LR\Filters::escapeHtmlText($person['last_name']) /* line 38 */ ?></td>
<?php
			if ($person["id_location"]) {
				?>                                <td><?php echo LR\Filters::escapeHtmlText($person['street_name']) /* line 40 */ ?> <?php
				echo LR\Filters::escapeHtmlText($person['street_number']) /* line 40 */ ?> <?php echo LR\Filters::escapeHtmlText($person['city']) /* line 40 */ ?> <?php
				echo LR\Filters::escapeHtmlText($person['zip']) /* line 40 */ ?></td>

<?php
			}
			else {
?>
                                <td>Nemá adresu</td>
<?php
			}
			?>                            <td><?php echo LR\Filters::escapeHtmlText($person['contact_count'] ? $person["contact_count"]: 0) /* line 45 */ ?></td>
                            <td>
                                <a class="btn btn-warning" href="<?php
			echo $router->pathFor("edit");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($person['id_person'])) /* line 47 */ ?>">Edituj</a>
                            </td>     
                            <td>
                                <form action="<?php
			echo $router->pathFor("delete");
?>" method="post" onsubmit="return confirm('Opravdu chcete smazat osobu?')">
                                    <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($person['id_person']) /* line 51 */ ?>">
                                    <input class="btn btn-danger" type="submit" value="Smaž">
                                </form>
                            </td>                       
                        </tr>
<?php
			$iterations++;
		}
?>
                </table>
            </div>
        </div>
    </div>
<?php
	}

}
