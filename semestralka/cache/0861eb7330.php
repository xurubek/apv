<?php
// source: add.latte

use Latte\Runtime as LR;

class Template0861eb7330 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>TITLE OF PAGE<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
        <h1>BODY CONTENT</h1>
        
<?php
		if (isset($warning)) {
			?>        <p><?php echo LR\Filters::escapeHtmlText($warning) /* line 10 */ ?></p>
<?php
		}
?>
        
        <form action="<?php
		echo $router->pathFor("add");
?>" method="post">
            <label for="first_name"><font color="red">*</font>Jméno: </label>
            <input id="first_name" type="text" name="first_name" value="<?php echo LR\Filters::escapeHtmlAttr($form['first_name']) /* line 15 */ ?>" required>
            <br>
            <label for="last_name"><font color="red">*</font>Příjmení: </label>
            <input id="last_name" type="text" name="last_name" value="<?php echo LR\Filters::escapeHtmlAttr($form['last_name']) /* line 18 */ ?>" required>
            <br>
            <label for="nickname"><font color="red">*</font>Přezdívka: </label>
            <input id="nickname" type="text" name="nickname" value="<?php echo LR\Filters::escapeHtmlAttr($form['nickname']) /* line 21 */ ?>">
            <br>
            <label for="birth_day">Datum narození: </label>
            <input id="birth_day" type="text" name="birth_day" placeholder="RRRR-MM-DD" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php
		echo LR\Filters::escapeHtmlAttr($form['birth_day']) /* line 24 */ ?>">
            <br>
            <label for="height">Výška: </label>
            <input id="height" type="number" name="height" value="<?php echo LR\Filters::escapeHtmlAttr($form['height']) /* line 27 */ ?>">
            <br>
            <input type="submit" value="Vytvoř">
            <br>
            <label>Povinné parametry jsou označeny symbolem *</label>
        </form>
    </div>
<?php
	}

}
