<?php
// source: index.latte

use Latte\Runtime as LR;

class Templateac6381e447 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['person'])) trigger_error('Variable $person overwritten in foreach on line 14');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>TITLE OF PAGE<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
        <h1>BODY CONTENT</h1>

        <table border="1">
            <tr>
                <th><b>Jméno</b></th>
                <th></b>Příjmení</b></th>
            </tr>
<?php
		$iterations = 0;
		foreach ($people as $person) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($person['first_name']) /* line 16 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($person['last_name']) /* line 17 */ ?></td>              
            </tr>            
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
