<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group('/auth', function () use ($app) {

    include('routes-person.php');
    //include('routes-location.php');
    //include('routes-meeting.php');

})->add(function (Request $request, Response $response, $next) {

    if (!empty($_SESSION['user'])) {
        return $next($request, $response);
    } else {


        return $response->withHeader('Location', $this->router->pathFor('login'));
    }
});


$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    $params = $request->getQueryParams();
    try {
        if (isset($params["filter"])) {
            $stmt = $this->db->prepare("select * from
(select id_location, id_person, first_name, last_name, nickname, count(id_contact) as \"contact_count\" from contact
right join person using (id_person)
group by person.id_person) as P
left join location using (id_location)
WHERE last_name ILIKE :filtered_name
order by last_name

");
            $stmt->bindValue(":filtered_name", $params["filter"] . "%");
            $stmt->execute();
        } else {
            $page = isset($params["page"]) ? max(1, intval($params["page"])) : 1;
            $limit = 10;

            $sql = "SELECT * FROM 
            (SELECT id_location, id_person, first_name, last_name, nickname, 
            COUNT(id_contact) AS \"contact_count\" FROM contact 
            right join person using (id_person)
            group by person.id_person) as P
            left join location using (id_location)";

            $stmt = $this->db->prepare($sql . " ORDER BY last_name LIMIT :lim OFFSET :off;");
            $stmt->bindValue(":lim", $limit);
            $stmt->bindValue(":off", ($page - 1) * $limit);
            $stmt->execute();

            $stmtCnt = $this->db->query("SELECT COUNT(*) AS cnt FROM person");
            $cnt = $stmtCnt->fetch();
            $maxpage = ceil($cnt["cnt"] / $limit);
            $tplVars["page"] = $page;
            $tplVars["maxpage"] = $maxpage;
        }
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    $tplVars['people'] = $stmt->fetchAll();

    $tplVars["message"] = "";
    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');

$app->post('/delete', function (Request $request, Response $response, $args) {
    $input = $request->getParsedBody();

    try {
        $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :idp");
        $stmt->bindValue(":idp", $input["id_person"]);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    return $response->withHeader('Location', $this->router->pathFor('index'));

})->setName('delete');

$app->get('/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {
        $stmt = $this->db->prepare("SELECT id_person, nickname, first_name, last_name, id_location, TO_CHAR(birth_day,'YYYY-MM-DD') birth_date_formated, height, gender, password, admin FROM person WHERE id_person = :id");
        $stmt->bindValue(":id", $params['id']);
        $stmt->execute();
        $person = $stmt->fetch();

        $stmt = $this->db->prepare("select * from location");
        $stmt->execute();
        $tplVars['location'] = $stmt->fetchAll();

        if ($person) {
            $tplVars["person"] = $person;
        } else {
            exit("Osoba nenalezena!");
        }
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }


    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }

    return $this->view->render($response, 'edit.latte', $tplVars);
})->setName('edit');

$app->post('/userEdit', function (Request $request, Response $response, $args) {
    $input = $request->getParsedBody();
    if (empty($_SESSION)) {
        exit("Editace uživatelů je povolena pouze po přihlášení.");
        //tato situace by nikdy neměla nastat, pokud nebude uživatel manipulovat s html nebo odeslanými packety. Ovládací prvky pro vyvolání této metody se zobrazí až po přihlášení.
    }

    if (empty($input["nickname"])) {
        $tplVars["message"] = " <br> <span style=\"color: #c82333\"> Vyplňte Uživatelské jméno </span> <br> <br> ";
        return $this->view->render($response, 'account.latte', $tplVars);
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE person SET first_name = :fn, last_name = :ln, gender = :g, id_location = :il , nickname = :nn, birth_day = TO_DATE(:bd, 'YYYY-MM-DD'), height = :h WHERE id_person = :idp");

            $birth_day = !empty($input["birth_day"]) ? $input["birth_day"] : null;
            $height = !empty($input["height"]) ? $input["height"] : null;

            $stmt->bindValue(":idp", $_SESSION['user']['id_person']);
            $stmt->bindValue(":fn", $input["first_name"]);
            $stmt->bindValue(":ln", $input["last_name"]);
            $stmt->bindValue(":nn", $input["nickname"]);
            $stmt->bindValue(":bd", $birth_day);
            $stmt->bindValue(":g", $input["gender"]);
            $stmt->bindValue(":h", $height);
            $stmt->bindValue(":il", $input["id_loc"]);

            $stmt->execute();

            $_SESSION['user']['first_name'] = $input["first_name"];
            $_SESSION['user']['last_name'] = $input["last_name"];
            $_SESSION['user']['nickname'] = $input["nickname"];
            $_SESSION['user']['birth_day'] = $birth_day;
            $_SESSION['user']['gender'] = $input["gender"];
            $_SESSION['user']['height'] = $height;
            $_SESSION['user']['id_location'] = $input["id_loc"];

            $_SESSION["message"] = '<br> <span style="color: #34ce57"> Editace byla úspěšně provedena </span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('account'));
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }
    }
})->setName('userEdit');

$app->post('/adminEdit', function (Request $request, Response $response, $args) {
    $input = $request->getParsedBody();
    $params = $request->getQueryParams();

    if (empty($_SESSION)) {
        exit("Editace uživatelů je povolena pouze po přihlášení.");
        //tato situace by nikdy neměla nastat, pokud nebude uživatel manipulovat s html nebo odeslanými packety. Ovládací prvky pro vyvolání této metody se zobrazí až po přihlášení.
    }

    if (!($_SESSION['user']['admin'])) {
        exit("Tato editace je povolena pouze pro administrátory.");
        //tato situace by nikdy neměla nastat, pokud nebude uživatel manipulovat s odeslanými packety. Ovládací prvky pro vyvolání této metody s editType-admin se zobrazí až po přihlášení jako admin.
    }

    if (empty($input["nickname"])) {
        $tplVars["message"] = "Vyplňte Uživatelské jméno";
        return $this->view->render($response, 'account.latte', $tplVars);
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE person SET first_name = :fn, last_name = :ln, gender = :g, id_location = :il, nickname = :nn, birth_day = TO_DATE(:bd, 'YYYY-MM-DD'), height = :h, admin = :ad WHERE id_person = :idp");

            $birth_day = !empty($input["birth_day"]) ? $input["birth_day"] : null;
            $height = !empty($input["height"]) ? $input["height"] : null;

            if (!isset($input["admin"])) {
                $input["admin"] = 'false';
            }

            print_r("Debug:" . $input["admin"]);

            $stmt->bindValue(":idp", $params['id']);
            $stmt->bindValue(":fn", $input["first_name"]);
            $stmt->bindValue(":ln", $input["last_name"]);
            $stmt->bindValue(":nn", $input["nickname"]);
            $stmt->bindValue(":bd", $birth_day);
            $stmt->bindValue(":h", $height);
            $stmt->bindValue(":g", $input["gender"]);
            $stmt->bindValue(":ad", $input["admin"]);
            $stmt->bindValue(":il", $input["id_loc"]);

            $stmt->execute();

            $_SESSION["message"] = '<br> <span style="color: #34ce57"> Editace byla úspěšně provedena </span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('edit') . '?id=' . $params['id']);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }
    }
})->setName('adminEdit');

$app->get('/login', function (Request $request, Response $response) {
    $tplVars["message"] = "";
    return $this->view->render($response, 'login.latte', $tplVars);
})->setName('login');

$app->post('/login', function (Request $request, Response $response) {
    $data = $request->getparsedBody();
    if (!empty($data['nickname']) && !empty($data['pass'])) {
        try {
            $stmt = $this->db->prepare("SELECT id_person, nickname, first_name, last_name, id_location, TO_CHAR(birth_day,'YYYY-MM-DD') birth_date_formated, height, gender, password, admin FROM person WHERE nickname = :n");
            $stmt->bindValue(":n", $data['nickname']);
            $stmt->execute();
            $user = $stmt->fetch();
            if ($user) {
                if (password_verify($data['pass'], $user['password'])) {
                    $_SESSION['user'] = $user;
                    return $response->withHeader('Location', $this->router->pathFor('account'));
                } else {
                    $tplVars["message"] = ' <span style="color: #c82333"> Špatné heslo </span> <br> <br>';
                    return $this->view->render($response, 'login.latte', $tplVars);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }
        exit("Neco je spatne");
    }
    $tplVars["message"] = ' <span style="color: #c82333"> Vyplňte všechna pole </span> <br> <br>';
    return $this->view->render($response, 'login.latte', $tplVars);
});


$app->post('/logout', function (Request $request, Response $response) {
    session_destroy();
    return $response->withHeader('Location', $this->router->pathFor('login'));
})->setName('logout');

$app->get('/register', function (Request $request, Response $response) {
    $tplVars["message"] = "";
    return $this->view->render($response, 'register.latte', $tplVars);
})->setName('register');

$app->post('/register', function (Request $request, Response $response) {
    $data = $request->getparsedBody();
    if (!empty($data['nickname']) && !empty($data['pass'] && !empty($data['pass-check']))) {
        try {
            if ($data['pass'] == $data['pass-check']) {
                $stmt = $this->db->prepare("INSERT INTO person (nickname, password, admin) VALUES (:n, :p, false)");
                $stmt->bindValue(":n", $data['nickname']);
                $stmt->bindValue(":p", password_hash($data['pass'], PASSWORD_DEFAULT));
                $stmt->execute();
                $tplVars["message"] = '<span style="color: #34ce57"> Úspěšně jste se zaregistroval(a). Nyní se můžete přihlásit. </span><br><br>';
                return $this->view->render($response, 'login.latte', $tplVars);
            } else {
                $tplVars["message"] = "Hesla se neshodují";
                return $this->view->render($response, 'register.latte', $tplVars);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $tplVars["message"] = $e->getMessage();
            if (strpos($e->getMessage(), 'duplicate key value violates unique constraint') !== false) {
                $tplVars["message"] = "Tento účet již existuje";
                return $this->view->render($response, 'register.latte', $tplVars);
            } else {
                $tplVars["message"] = "Registrace se nezdařila s chybou: " . $e->getMessage();
                return $this->view->render($response, 'register.latte', $tplVars);
            }
        }
    }
    $tplVars["message"] = "Vyplňte prosím všechna pole";
    return $this->view->render($response, 'register.latte', $tplVars);
})->setName('register');

$app->get('/account', function (Request $request, Response $response) {
    //print_r($_SESSION);

    try {
        $stmt = $this->db->prepare("select * from location");
        $stmt->execute();
        $tplVars['location'] = $stmt->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }
    return $this->view->render($response, 'account.latte', $tplVars);
})->setName('account');

$app->get('/addresses', function (Request $request, Response $response) {
    $tplVars['message'] = "";
    $params = $request->getQueryParams();

    try {

        $page = isset($params["page"]) ? max(1, intval($params["page"])) : 1;
        $limit = 10;

        $stmt = $this->db->prepare("SELECT * from location LIMIT :lim OFFSET :off;");
        $stmt->bindValue(":lim", $limit);
        $stmt->bindValue(":off", ($page - 1) * $limit);
        $stmt->execute();
        $tplVars['addresses'] = $stmt->fetchAll();

        $stmtCnt = $this->db->query("SELECT COUNT(*) AS cnt FROM location");
        $cnt = $stmtCnt->fetch();
        $maxpage = ceil($cnt["cnt"] / $limit);
        $tplVars["page"] = $page;
        $tplVars["maxpage"] = $maxpage;
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }
    return $this->view->render($response, 'addresses.latte', $tplVars);
})->setName('addresses');

$app->post('/deleteAddr', function (Request $request, Response $response) {
    $input = $request->getParsedBody();

    if (empty($_SESSION)) {
        $_SESSION["message"] = ' <span style="color: #c82333"> Tuto akce je možné provést pouze po přihlášení </span> <br> <br>';
        return $response->withHeader('Location', $this->router->pathFor('addresses'));
    }

    try {
        $stmt = $this->db->prepare("DELETE FROM location WHERE id_location = :id");
        $stmt->bindValue(":id", $input["id_location"]);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

        if (strpos($e->getMessage(), 'violates foreign key constraint') !== false) {
            $_SESSION["message"] = ' <span style="color: #c82333"> Tato adresa je používaná, proto nemůže být odstraněna. </span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('addresses'));
        } else {
            $_SESSION["message"] = ' <span style="color: #c82333"> Odstranění se se nezdařilo s chybou: ' . $e->getMessage() . '</span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('addresses'));
        }
    }
    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Adresa byla úspěšně odstraněna </span> <br> <br> ';

    return $response->withHeader('Location', $this->router->pathFor('addresses'));
})->setName('deleteAddr');

$app->get('/addAddr', function (Request $request, Response $response) {
    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }
    return $this->view->render($response, 'addAddr.latte', $tplVars);
})->setName('addAddr');

$app->post('/addAddr', function (Request $request, Response $response) {
    $data = $request->getparsedBody();

    if (empty($_SESSION)) {
        exit("Tuto akci může vykonat pouze přihlášený uživatel.");
    }

    try {
        $stmt = $this->db->prepare("INSERT INTO location (city, street_name, street_number, zip, country, name) VALUES (:ci, :sna, :snu, :z, :co, :n)");
        $stmt->bindValue(":ci", $data['city']);
        $stmt->bindValue(":sna", $data['street_name']);
        $stmt->bindValue(":snu", $data['street_number']);
        $stmt->bindValue(":z", $data['zip']);
        $stmt->bindValue(":co", $data['country']);
        $stmt->bindValue(":n", $data['name']);
        $stmt->execute();
    } catch (Exception $e) {
        exit($e->getMessage());
    }
    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Adresa byla úspěšně přidána </span> <br> <br>';
    return $response->withHeader('Location', $this->router->pathFor('addAddr'));
})->setName('addAddr');

$app->get('/editAddr', function (Request $request, Response $response) {
    $params = $request->getQueryParams();

    try {
        $stmt = $this->db->prepare("select * from location where id_location = :id ");
        $stmt->bindValue(":id", $params['id']);
        $stmt->execute();
        $tplVars['location'] = $stmt->fetch();
    } catch (Exception $e) {
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }

    return $this->view->render($response, 'editAddr.latte', $tplVars);
})->setName('editAddr');

$app->post('/editAddr', function (Request $request, Response $response) {
    $data = $request->getparsedBody();
    $params = $request->getQueryParams();

    if (empty($_SESSION)) {
        exit("Tuto akci může vykonat pouze přihlášený uživatel.");
    }

    try {
        $stmt = $this->db->prepare("UPDATE location SET city= :ci , street_name= :sna , street_number= :snu , zip= :z , country = :co , name = :n WHERE id_location = :id");
        $stmt->bindValue(":ci", $data['city']);
        $stmt->bindValue(":sna", $data['street_name']);
        $stmt->bindValue(":snu", $data['street_number']);
        $stmt->bindValue(":z", $data['zip']);
        $stmt->bindValue(":co", $data['country']);
        $stmt->bindValue(":n", $data['name']);
        $stmt->bindValue(":id", $params['id']);
        $stmt->execute();
    } catch (Exception $e) {
        exit($e->getMessage());
    }
    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Adresa byla úspěšně změněna </span> <br> <br>';
    return $response->withHeader('Location', $this->router->pathFor('addresses'));
})->setName('editAddr');

//****************************************************************************************************************************************************************************************************************************************************************************************************************************


$app->get('/meetings', function (Request $request, Response $response) {
    $params = $request->getQueryParams();

    try {
        $page = isset($params["page"]) ? max(1, intval($params["page"])) : 1;
        $limit = 10;

        $stmt = $this->db->prepare("SELECT city, street_name, street_number, zip, country, id_meeting, name, TO_CHAR(start,'YYYY-MM-DD') start_formated, description, duration, id_location from meeting 
left join location using(id_location) LIMIT :lim OFFSET :off;");
        $stmt->bindValue(":lim", $limit);
        $stmt->bindValue(":off", ($page - 1) * $limit);
        $stmt->execute();
        $tplVars['meetings'] = $stmt->fetchAll();

        $stmtCnt = $this->db->query("SELECT COUNT(*) AS cnt FROM location");
        $cnt = $stmtCnt->fetch();
        $maxpage = ceil($cnt["cnt"] / $limit);
        $tplVars["page"] = $page;
        $tplVars["maxpage"] = $maxpage;
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }
    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');

$app->post('/deleteMeeting', function (Request $request, Response $response) {
    $input = $request->getParsedBody();

    if (empty($_SESSION)) {
        $_SESSION["message"] = ' <span style="color: #c82333"> Tuto akce je možné provést pouze po přihlášení </span> <br> <br>';
        return $response->withHeader('Location', $this->router->pathFor('meetings'));
    }

    try {
        $stmt = $this->db->prepare("DELETE FROM meeting WHERE id_meeting = :id");
        $stmt->bindValue(":id", $input["id_meeting"]);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

        if (strpos($e->getMessage(), 'violates foreign key constraint') !== false) {
            $_SESSION["message"] = ' <span style="color: #c82333"> Tato schůzka je používaná, proto nemůže být odstraněna. </span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('meetings'));
        } else {
            $_SESSION["message"] = ' <span style="color: #c82333"> Odstranění se se nezdařilo s chybou: ' . $e->getMessage() . '</span> <br> <br>';
            return $response->withHeader('Location', $this->router->pathFor('meetings'));
        }
    }
    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Adresa byla úspěšně odstraněna </span> <br> <br> ';

    return $response->withHeader('Location', $this->router->pathFor('meetings'));
})->setName('deleteMeeting');

$app->get('/addMeeting', function (Request $request, Response $response) {
    try {
        $stmt = $this->db->prepare("select * from location");
        $stmt->execute();
        $tplVars['location'] = $stmt->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }
    return $this->view->render($response, 'addMeeting.latte', $tplVars);
})->setName('addMeeting');

$app->post('/addMeeting', function (Request $request, Response $response) {
    $data = $request->getparsedBody();

    if (empty($_SESSION)) {
        exit("Tuto akci může vykonat pouze přihlášený uživatel.");
    }

    try {
        $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location) VALUES (TO_DATE(:st, 'YYYY-MM-DD'), :de, :du, :lo)");
        $stmt->bindValue(":st", $data['start']);
        $stmt->bindValue(":de", $data['description']);
        $stmt->bindValue(":du", $data['duration']);
        $stmt->bindValue(":lo", $data['id_location']);
        $stmt->execute();
    } catch (Exception $e) {
        exit($e->getMessage());
    }

    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Schůka byla úspěšně přidána </span> <br> <br>';
    return $response->withHeader('Location', $this->router->pathFor('addMeeting'));
})->setName('addMeeting');

$app->get('/editMeeting', function (Request $request, Response $response) {
    $params = $request->getQueryParams();

    try {
        $stmt = $this->db->prepare("SELECT id_meeting, TO_CHAR(start,'YYYY-MM-DD') start_formated, description, duration, id_location from meeting where id_meeting = :id ");
        $stmt->bindValue(":id", $params['id']);
        $stmt->execute();
        $tplVars['meeting'] = $stmt->fetch();

        $stmt = $this->db->prepare("select * from location");
        $stmt->execute();
        $tplVars['location'] = $stmt->fetchAll();

    } catch (Exception $e) {
        exit($e->getMessage());
    }

    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        $tplVars['message'] = $_SESSION['message'];
        $_SESSION['message'] = '';
    } else {
        $tplVars['message'] = '';
    }

    //print_r($tplVars['meeting']);

    return $this->view->render($response, 'editMeeting.latte', $tplVars);
})->setName('editMeeting');

$app->post('/editMeeting', function (Request $request, Response $response) {
    $data = $request->getparsedBody();
    $params = $request->getQueryParams();

    if (empty($_SESSION)) {
        exit("Tuto akci může vykonat pouze přihlášený uživatel.");
    }

    try {
        $stmt = $this->db->prepare("UPDATE meeting SET  start = TO_DATE(:st, 'YYYY-MM-DD'), description= :de , duration= :du , id_location = :lo WHERE id_location = :id");
        $stmt->bindValue(":st", $data['start']);
        $stmt->bindValue(":de", $data['description']);
        $stmt->bindValue(":du", $data['duration']);
        $stmt->bindValue(":lo", $data['id_location']);
        $stmt->bindValue(":id", $params['id']);
        $stmt->execute();
    } catch (Exception $e) {
        exit($e->getMessage());
    }
    $_SESSION["message"] = '<br> <span style="color: #34ce57"> Adresa byla úspěšně změněna </span> <br> <br>';
    return $response->withHeader('Location', $this->router->pathFor('editMeeting').'?id='.$params['id'] );
})->setName('editMeeting');

//print_r("debug");

